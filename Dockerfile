FROM ubuntu:18.04

MAINTAINER Dockerfiles

# Install required packages and remove the apt packages cache when done.

RUN apt-get update && \
    apt-get upgrade -y && \ 	
    apt-get install -y \
	git \
	python3 \
	python3-dev \
	python3-setuptools \
	python3-pip \
	supervisor \
        locales && \
	pip3 install -U pip setuptools && \
   locale-gen "ru_RU.UTF-8" && \
   update-locale LANG=ru_RU.UTF-8 && \ 
   export LC_ALL=ru_RU.UTF-8 && \
   rm -rf /var/lib/apt/lists/*

RUN pip install --upgrade pip

RUN echo "" > /etc/supervisor/conf.d/supervisor-app.conf && \
    echo "[program:app-django]" >> /etc/supervisor/conf.d/supervisor-app.conf && \
    echo "command = python3 /app/manage.py runserver 0.0.0.0:8000" >> /etc/supervisor/conf.d/supervisor-app.conf && \
    echo "" >> /etc/supervisor/conf.d/supervisor-app.conf && \
    echo "[program:app-celery]" >> /etc/supervisor/conf.d/supervisor-app.conf && \
    echo "command = celery -A app worker" >> /etc/supervisor/conf.d/supervisor-app.conf && \
    echo "environment=LANG=\"ru_RU.UTF-8\",LC_ALL=\"ru_RU.UTF-8\"" >> /etc/supervisor/conf.d/supervisor-app.conf

ADD ./app /app
WORKDIR /app/
RUN pip3 install -r requirements.txt

RUN echo "" >> /root/.bashrc && \
    echo "export LC_ALL=\"ru_RU.UTF-8\"" >> /root/.bashrc
RUN echo "" >> /root/.bashrc && \
    echo "alias p='python3'" >> /root/.bashrc

EXPOSE 8000
CMD ["/bin/bash", "-c", "python3 /app/dbwait.py ; python3 /app/manage.py migrate ; python3 /app/initial_data.py ; supervisord -n"]
