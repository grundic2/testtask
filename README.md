# python Django Celery testtask

This task uses:
 - Python 3.6
 - Django 2.2
 - Django Rest Framework
 - Celery
 - PostgreSQL Pro
 - Docker + docker‑compose

# Fast start
  - git clone repository
  - run **docker-compose up autotests** - to check code
  - run **docker-compose up runserver** - to run server at http://localhost:8000/

# URLS
 - **/admin/** - Django admin access (default SU login-pass is **admin-admin** as set at docker-compose.yml service env)
 - **/messages/** - Messages API
