# coding=utf-8
#!/usr/bin/env python
import sys
import os
EOR_PROJECT_PATH = os.path.abspath(__file__)
sys.path.insert(0, os.path.dirname(os.path.dirname(EOR_PROJECT_PATH)))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings")
import django
django.setup()

from app.models import *
import random

def main():
    if True: # creating cuperuser
        admin = [
            os.environ.get('DJANGO_ADMIN_LOGIN', 'admin'),
            os.environ.get('DJANGO_ADMIN_PASSWORD', 'admin'),
            os.environ.get('DJANGO_ADMIN_EMAIL', 'no@na.me'),
        ]
        
        if not User.objects.filter(username=admin[0]).exists():
            User.objects.create_superuser(username=admin[0], password=admin[1], email=admin[2])
    
    if False: # creating content
        content = [ # title, data
            ['#16 CS:GO - Почти эйсы', {'type': 'video', 'url': 'https://www.youtube.com/watch?v=Bf_u4OW2v-Y', 'sub': 'https://www.youtube.com/watch?v=Bf_u4OW2v-Y#sub'}],
            ['Вторая мировая война. Золото рейха | History Lab', {'type': 'video', 'url': 'https://www.youtube.com/watch?v=C2jWy8mraa4', 'sub': 'https://www.youtube.com/watch?v=C2jWy8mraa4#sub'}],
            ['Фоновая музыка для учебы / музыка для концентрации внимания [ стади виз ми музыка lofi hip hop ]', {'type': 'video', 'url': 'https://www.youtube.com/watch?v=SiLVhVZKaOw', 'sub': 'https://www.youtube.com/watch?v=SiLVhVZKaOw#sub'}],
            ['04 - Metallica - The Day That Never Comes', {'type': 'audio', 'bitrate': 320}],
            ['Kenji Kawai - Utai IV Reawakening', {'type': 'audio', 'bitrate': 320}],
            ['Kermode - Funk That', {'type': 'audio', 'bitrate': 320}],
            ['Lorem ipsum', {'type': 'text', 'text': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'}],
            ['Ваш шедевр готов!', {'type': 'text', 'text': 'Задача организации, в особенности же новая модель организационной деятельности позволяет оценить значение систем массового участия. Не следует, однако забывать, что постоянное информационно-пропагандистское обеспечение нашей деятельности обеспечивает широкому кругу (специалистов) участие в формировании форм развития. С другой стороны консультация с широким активом в значительной степени обуславливает создание существенных финансовых и административных условий. Идейные соображения высшего порядка, а также укрепление и развитие структуры представляет собой интересный эксперимент проверки направлений прогрессивного развития. Товарищи! новая модель организационной деятельности влечет за собой процесс внедрения и модернизации существенных финансовых и административных условий.'}],
            ['Съешь ещё этих мягких французских булок, да выпей же чаю', {'type': 'text', 'text': 'Широкая электрификация южных губерний даст мощный толчок подъёму сельского хозяйства'}],
        ]
        
        for title, data in content:
            if not PageContent.objects.filter(title=title).exists():
                PageContent.objects.create(title=title, data=data)
    
    if False: # creating pages
        contents = list(PageContent.objects.all())
        for _ in range(50):
            title = 'Страница %s %s' % (
                ''.join(random.choices('АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЭЮЯ', k=2)),
                str(Page.objects.count())
            )
            if not Page.objects.filter(title=title).exists():
                page = Page.objects.create(title=title)
                [page.content.add(c) for c in random.sample(contents, random.randint(1,4))]

if __name__ == "__main__":
    main()
