# coding=utf-8
#!/usr/bin/env python
import sys
import os
EOR_PROJECT_PATH = os.path.abspath(__file__)
sys.path.insert(0, os.path.dirname(os.path.dirname(EOR_PROJECT_PATH)))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings")
import django
django.setup()

from app.models import *
import time

def dbwait():
    from django.db import connections
    from django.db.utils import OperationalError
    db_conn = connections['default']
    connected = False
    while not connected:
        try:
            c = db_conn.cursor()
        except OperationalError:
            connected = False
            print('Waiting DB connection')
            time.sleep(1)
        else:
            connected = True

if __name__ == "__main__":
    dbwait()
