# coding=utf-8
#!/usr/bin/env python
import sys
import os
EOR_PROJECT_PATH = os.path.abspath(__file__)
sys.path.insert(0, os.path.dirname(os.path.dirname(EOR_PROJECT_PATH)))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings")
import django
django.setup()

from app import settings
from app.models import *

from celery import Celery
app = Celery('app', broker=settings.CELERY_BROKER_URL)
app.config_from_object('django.conf:settings', namespace='CELERY')

@app.task
def message_read(pk):
    Message.objects.filter(pk=pk).update(readed=True)
    return True

@app.task(rate_limit='10/m')
def message_send(pk):
    Message.objects.filter(pk=pk).update(sent=True)
    return True
