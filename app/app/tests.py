from django.test import TestCase, Client
from app.models import Message
from django.core.exceptions import ValidationError
from rest_framework.reverse import reverse
import json
import random

class MessagesListApiTest(TestCase):
    def setUp(self):
        from app.celery import app as celeryapp
        celeryapp.conf.update(CELERY_ALWAYS_EAGER=True)
        
        for _ in range(50):
            message = Message.objects.create(title='Bar'+str(_), text='Foo'+str(_))
    
    def test_celery_eager(self):
        from app.celery import message_read
        message = Message.objects.first()
        self.assertEqual(message.readed, False)
        message_read.delay(message.pk)
        self.assertEqual(Message.objects.get(pk=message.pk).readed, True)
        Message.objects.filter(pk=message.pk).update(readed=False)
    
    def test_list(self):
        messages = Message.objects.all().order_by('-id')
        path = reverse('message-list') + '?format=json'
        
        c = Client()
        response = c.get(path)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode())
        self.assertEqual(data['count'], messages.count())
        
        self.assertEqual(data['previous'], None)
        pages_offset = 0
        
        self.assertNotEqual(len(data['results']), 0)
        pages_chunk = len(data['results'])
        
        _messages = list(messages.values('pk', 'title'))
        
        for i in range(pages_chunk):
            self.assertEqual(len(data['results'][i].keys()), 2)
            self.assertEqual(data['results'][i]['title'], _messages[pages_offset+i]['title'])
        
        self.assertEqual(messages.filter(readed=True).count(), 0)

class MessagesRetriveApiTest(TestCase):
    def setUp(self):
        from app.celery import app as celeryapp
        celeryapp.conf.update(CELERY_ALWAYS_EAGER=True)
        
        for _ in range(50):
            message = Message.objects.create(title='Bar'+str(_), text='Foo'+str(_))
    
    def test_celery_eager(self):
        from app.celery import message_read
        message = Message.objects.first()
        self.assertEqual(message.readed, False)
        message_read.delay(message.pk)
        self.assertEqual(Message.objects.get(pk=message.pk).readed, True)
        Message.objects.filter(pk=message.pk).update(readed=False)
    
    def test_retrive(self):
        message = Message.objects.first()
        path = reverse('message-list') + str(message.pk) + '/?format=json'
        self.assertEqual(message.readed, False)
        
        c = Client()
        response = c.get(path)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode())
        message = Message.objects.get(pk=message.pk)
        self.assertEqual(message.readed, True)
        
        self.assertEqual(data['title'], message.title)
        self.assertEqual(data['text'], message.text)
