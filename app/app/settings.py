import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'p=((+45a8vy$=^hwnse)#!p_fx@233+xer#-^p98i-%@^il9f-'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = int(os.environ.get('DJANGO_DEBUG', '0')) > 0

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'app'
]

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 10,
    'DEFAULT_RENDERER_CLASSES': [
        'rest_framework.renderers.BrowsableAPIRenderer',
        'rest_framework.renderers.JSONRenderer',
        'rest_framework_csv.renderers.CSVRenderer',
    ]
}

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'app.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            'templates/'
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# Database

DATABASES = {
    'default': {
        'ENGINE':   'django.db.backends.postgresql_psycopg2',
        'NAME':     os.environ.get('DJANGO_DB_NAME', 'app'),
        'USER':     os.environ.get('DJANGO_DB_USER', 'postgres'),
        'PASSWORD': os.environ.get('DJANGO_DB_PASSWORD', 'secret'),
        'HOST':     os.environ.get('DJANGO_DB_HOST', 'db'),
        'PORT':     os.environ.get('DJANGO_DB_PORT', '5432'),
        'ATOMIC_REQUESTS': True,
    }
}

CELERY_BROKER_URL = 'sqla+postgresql://%s:%s@%s:%s/%s' % (
    os.environ.get('DJANGO_DB_USER', 'postgres'),
    os.environ.get('DJANGO_DB_PASSWORD', 'secret'),
    os.environ.get('DJANGO_DB_HOST', 'db'),
    os.environ.get('DJANGO_DB_PORT', '5432'),
    os.environ.get('DJANGO_DB_NAME', 'app')
)

# Password validation

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization

LANGUAGE_CODE = 'ru-ru'
LANGUAGES = (
  ('ru-ru', 'Russian'),
  ('en-en', 'English')
)

TIME_ZONE = 'Europe/Moscow'
TIME_FORMAT = '%Y/%m/%d %H:%M:%S'
DATE_FORMAT = '%Y/%m/%d'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SYSTEM_URL = os.environ.get('DJANGO_URL_SYSTEM', 'http://localhost/')

# Static files (CSS, JavaScript, Images)

STATIC_URL = '/static/'
