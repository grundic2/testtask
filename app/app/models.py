from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ValidationError
import json

class Message(models.Model): # сообщение
    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        ordering = ['-id']
    
    title = models.CharField(max_length=250, null=False, verbose_name='Заголовок')
    text = models.TextField(null=True, verbose_name='Тело сообщения')
    sent = models.BooleanField(default=False, verbose_name='Отправлено')
    readed = models.BooleanField(default=False, verbose_name='Прочитано')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    modified = models.DateTimeField(auto_now=True, verbose_name='Обновлено')
    
    def __unicode__(self):
        return self.title
    
    def __str__(self):
        return self.__unicode__()
