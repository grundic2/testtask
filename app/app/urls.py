from django.urls import path, include
from django.conf.urls import url
from .views import MessageViewSet
from django.contrib import admin as django_admin
from rest_framework import routers

router = routers.DefaultRouter()
router.register('messages', MessageViewSet)

urlpatterns = [
    url(r'^admin/', django_admin.site.urls),
    path('', include(router.urls)),
]

