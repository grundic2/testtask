from app.models import *
from rest_framework import serializers, viewsets
from app.celery import message_read, message_send
from rest_framework.response import Response

class MessageListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Message
        fields = ['url', 'title']

class MessageInfoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Message
        fields = ['url', 'title', 'text', 'readed']

class MessageFullInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ['pk', 'title', 'text', 'sent', 'readed', 'created']

class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    http_method_names = ['get', 'post', 'delete']
    
    def list(self, request, *args, **kwargs):
        if request.accepted_renderer.format == 'csv':
            data = self.queryset.order_by('created')
            data = [MessageFullInfoSerializer(d, context={'request': request}).data for d in data]
            return Response(data)
        
        return super(MessageViewSet, self).list(request, *args, **kwargs)
    
    def retrieve(self, request, *args, **kwargs):
        obj = self.get_object()
        message_read.delay(obj.pk)
        return super(MessageViewSet, self).retrieve(request, *args, **kwargs)
    
    def create(self, validated_data):
        obj = Message.objects.create(**validated_data)
        message_send.delay(obj.pk)
        return obj
    
    def get_serializer_class(self):
        if self.action == 'list':
            return MessageListSerializer
        if self.action == 'retrieve':
            return MessageInfoSerializer
        return MessageListSerializer
